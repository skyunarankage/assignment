/**
 * Created by Adriel Noach Premer on 14-04-2017.
 */
import React from 'react';
import patients from '../data.json';

import {Api} from "../api/api.jsx";

export class Patients extends React.Component {
    constructor(props) {
        super(props);
        let patientss = patients;
        //setting all the variables which we will use.
        this.state={
            patients: patientss.length,
            patientList: patientss,
            name: "Name",
            email: "Email",
            number: "Number",
            birth: "Birth",
            Changing: 0,
            medid: 0,
            patid: 0,
            searchpatient: "",
            searchmed: "",
            medList:{},
            assign:false
        }
    }

    render() {
        return (
            <div>
                <label>
                    <input name="searchpatient" type="text" placeholder="Search On A Patient" onChange={this._saveit.bind(this)}/>
                    <button className="solo-btn" onClick={this._CreateNew.bind(this)}>
                        Create New
                    </button>
                </label>
                    <div>
                    {this.state.patientList.map((patient) =>
                        <div>
                            {patient.name.includes(this.state.searchpatient) &&
                                <div key={patient.id.toString()}>
                                    {patient.name}


                                    {this.state.patientidwatch == patient.id &&
                                    <div className="patient">
                                        {patient.name}<br/>
                                        {patient.email}<br/>
                                        {patient.phone}<br/>
                                        {patient.born}<br/>
                                        The Assigned Medicin

                                        {patient.perscribed.length > 1 && patient.perscribed.map((med) =>
                                            <div>
                                                <button className="item" onClick={this._SeeMed.bind(this, patient.id, med.id)}>
                                                    <ul>
                                                        <li>
                                                            {med.name}
                                                        </li>
                                                    </ul>
                                                </button>
                                            </div>
                                        )}

                                        {patient.perscribed.id &&
                                            <div>
                                                <button className="item" onClick={this._SeeMed.bind(this, patient.id, 1)}>
                                                    <ul>
                                                        <li>
                                                            {patient.perscribed.name}
                                                        </li>
                                                    </ul>
                                                </button>
                                            </div>
                                        }

                                        <br/>
                                        {this.state.patid == this.state.patientidwatch &&
                                        <div>{this.state.patientList[this.state.patientidwatch - 1].perscribed[this.state.medid - 1].name}
                                            <button onClick={this._PrepareChange.bind(this, patient.id)}>
                                                Edit Details
                                            </button>
                                        </div>
                                        }
                                        <button onClick={this._PrepareChange.bind(this, patient.id)}>
                                            Edit Details
                                        </button>
                                        <button onClick={this._onDeletePatient.bind(this)}>
                                            Delete
                                        </button>
                                        <button onClick={this._PrepareAssign.bind(this, patient.id)}>
                                            Add Med
                                        </button>
                                    </div>
                                    }
                                    {this.state.Changing == patient.id &&
                                    this.state.patientidwatch == this.state.Changing &&
                                    <div className="patientForm">
                                        <form onSubmit={this._onSubmit.bind(this)}>
                                            <legend>{patient.name}</legend>
                                            <fieldset>
                                                <label>
                                                    <input name="name" type="text" placeholder={this.state.name}
                                                           onChange={this._saveit.bind(this)}/>
                                                </label>
                                                <label>
                                                    <input name="email" type="text" placeholder={this.state.email}
                                                           onChange={this._saveit.bind(this)}/>
                                                </label>
                                                <label>
                                                    <input name="number" type="text" placeholder={this.state.number}
                                                           onChange={this._saveit.bind(this)}/>
                                                </label>
                                                <label>
                                                    <input name="birth" type="text" placeholder={this.state.birth}
                                                           onChange={this._saveit.bind(this)}/>
                                                </label>
                                                <input type="submit" value="Submit"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    }


                                    <button onClick={this._seepatient.bind(this, patient.id)}>
                                        See more Details
                                    </button>
                                </div>
                            }
                        </div>
                    )}
                    </div>


                {this.state.assign &&
                    <div className="medicin">
                <label>
                    <input name="searchmed" type="text" placeholder="Search On A Meds" onChange={this._saveit.bind(this)}/>
                </label>
                        <hr/>

                    {this.state.medList.length > 1 &&
                        <div>
                    {this.state.medList.map((med) =>
                        <div key={med.id.toString()}>
                            {med.productName}

                            <button onClick={this._assign.bind(this,med.productName)}>
                                Assign Med
                            </button>
                        </div>
                    )}
                </div>
                }
            </div>}</div>
        )}

    //puts med information into the array of perscriptions assigned to a specific user.
    _assign(thatmed){
        let patientLi=this.state.patientList;
        let i=1;

        while (patientLi[this.state.patientidwatch-1].perscribed[i-1]) {
            i++;
        }
        let newMed = {
            "id": i,
            "name": thatmed
        };
        if( Object.prototype.toString.call( patientLi[this.state.patientidwatch-1].perscribed ) === '[object Array]' ) {
            patientLi[this.state.patientidwatch-1].perscribed.push(newMed);
            this.setState({patientList: patientLi});
        }
        else{
            if(patientLi[this.state.patientidwatch-1].perscribed.id){
                let oldMed = {
                    "id": patientLi[this.state.patientidwatch-1].perscribed.id,
                    "name": patientLi[this.state.patientidwatch-1].perscribed.name
                };
                patientLi[this.state.patientidwatch-1].perscribed= new Array(oldMed,newMed);
            }
            else{
                patientLi[this.state.patientidwatch-1].perscribed=newMed;
            }

            this.setState({patientList: patientLi});
        }
        this.setState({assign:false})
    }

    //saves the information put in the forms
    _saveit(event) {
        let value = event.target.value;
        let name = event.target.name;
        this.setState({
            [name]: value
        });
        if(name=="searchmed"){
                let that = this;
                Api.search(value, function (err, res) {
                    let meds = res.body;
                    that.setState({
                        medList: meds,
                    });
                });
        }
    }

    //toggles show patient
    _seepatient(id){
        this.setState({
            patientidwatch: id
        });
        this.setState({assign:false});
        this.setState({
            Changing: 0
        });

    }


    //toggles changes user information
    _onSubmit(event){
        event.preventDefault();
        let patientList=this.state.patientList;
        patientList[this.state.patientidwatch-1].name=this.state.name;
        patientList[this.state.patientidwatch-1].email=this.state.email;
        patientList[this.state.patientidwatch-1].phone=this.state.number;
        patientList[this.state.patientidwatch-1].born=this.state.birth;

        this.setState({patientList: patientList})
    }

    //removes the patient of chosen id from the array
    _onDeletePatient(id){
        let patientList=this.state.patientList;
        delete patientList[this.state.patientidwatch-1];
        this.setState({patientList: patientList})
    }

    //creates a new patient and inserts it into the patient array
    _CreateNew(){
        let patientLi=this.state.patientList;

        let i=1;
        console.log(i);
        while (patientLi[i-1]) {
            console.log(i);
            i++;
        }
        console.log(i);
        let newUser = {
            id: i,
            name: "new user",
            email: "email",
            phone: "phone",
            born: "birthdate",
            "perscribed": {}
        };
        patientLi.push(newUser);
        this.setState({patientList: patientLi})
    }


    _SeeMed(patid,medid){
        this.setState({medid:medid,patid:patid})
    }

    _PrepareAssign(){
        this.setState({assign:true});
        this.setState({
            Changing: 0
        });

    }
    _PrepareChange(id){
        this.setState({
            Changing: id
        });
        this.setState({assign:false});
    }


}