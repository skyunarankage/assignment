/**
 * Created by Adriel Noach Premer on 14-04-2017.
 */
import React from 'react';
//the routing
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

//added css, it works
import styles from './App.less';

//added links to other parts
import {Patients} from './Patients/patiens.jsx';
import {About} from './About/about.jsx';


export class App extends React.Component {
    constructor(props) {
        super(props);
    }

    //rendering with the router in focus.
    render() {
        return (
            <Router>
                <div>
                    <div className="wrapper">
                        <div>
                            <nav>
                                <div className="navCont">
                                    <div>WorkOpt</div>
                                    <div><Link to={'/Patients'}>Frontpage</Link></div>
                                    <div><Link to={'/About'}>Readme</Link></div>
                                </div>
                            </nav>
                        </div>
                        <div className="content">
                            <Route exact={true} path="/" component={Patients} />
                            <Route path="/Patients" component={Patients} />
                            <Route path="/About" component={About}/>
                        </div>
                     </div>
                </div>
            </Router>
        )
    }
}