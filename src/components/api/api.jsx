import request from 'superagent';
export class Api {
    //Requesting to get some information back and giving it to the function which called it.
    static search(name, callback) {
        request.get('https://fest-searcher.herokuapp.com/api/fest/s/' + name).end(callback);
    }

}