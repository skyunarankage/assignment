import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';

import {App} from './components/App.jsx';

const render = (component) => {
    ReactDOM.render(
            <AppContainer>
                <App/>
            </AppContainer>,
        document.getElementById('root')
    );
};

render(App);

//settign up that only the app shall be reloaded not more.
if (module.hot) {
    module.hot.accept('./components/App.jsx', () => {render(App)});
}